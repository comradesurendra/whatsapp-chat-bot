$(document).ready(function() {

    //change the name of hospital 
   
    function getUrlVars()
    {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
    }
    var res = getUrlVars()
    var phone = res.phone
    var img = res.img
    var hospitalName = res.name
    var show = res.show
    var bot = `
        <div class="container">
            <div class="chat-container">
            <div class="chat-header">
                <div class="logo">
                <img src=${img} class="logo-img">
                </div>
                <div class="small-icon2"></div>
                <div class="hosp-name">${hospitalName}</div>
                <div class="hosp-name-text">Typically replies instantly</div>
                <div class="close">
                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="18" viewBox="0 0 20.092 20.051">
                <g id="Group_10778" data-name="Group 10778" transform="translate(-1110.96 -113)">
                    <path id="Path_5041" data-name="Path 5041" d="M18.342,15.994,25.5,8.85a1.672,1.672,0,1,0-2.364-2.365l-7.143,7.161L8.851,6.486A1.672,1.672,0,1,0,6.487,8.85l7.16,7.144-7.16,7.144A1.672,1.672,0,1,0,8.851,25.5l7.143-7.161L23.137,25.5A1.672,1.672,0,1,0,25.5,23.139Z" transform="translate(1105.006 107.004)" fill="#fff"/>
                </g>
                </svg>
                </div>
            </div>
            <div class="chat-area">
                <div class="chat-point"></div>
                <div class="chat-box">
                    <div class="chat-tital">${hospitalName}</div>
                    <div class="chat-text">Hi there 👋 </br></br>How can i help you?</div>
                </div>
            </div>
            <div class="whatsapp">
                    <div class="whatsapp-button">
                    <div class="chat-icon">
                    <svg viewBox="0 0 90 90" fill="rgb(255, 255, 255)" width="32" height="32">
                    <path d="M90,43.841c0,24.213-19.779,43.841-44.182,43.841c-7.747,0-15.025-1.98-21.357-5.455L0,90l7.975-23.522   c-4.023-6.606-6.34-14.354-6.34-22.637C1.635,19.628,21.416,0,45.818,0C70.223,0,90,19.628,90,43.841z M45.818,6.982   c-20.484,0-37.146,16.535-37.146,36.859c0,8.065,2.629,15.534,7.076,21.61L11.107,79.14l14.275-4.537   c5.865,3.851,12.891,6.097,20.437,6.097c20.481,0,37.146-16.533,37.146-36.857S66.301,6.982,45.818,6.982z M68.129,53.938   c-0.273-0.447-0.994-0.717-2.076-1.254c-1.084-0.537-6.41-3.138-7.4-3.495c-0.993-0.358-1.717-0.538-2.438,0.537   c-0.721,1.076-2.797,3.495-3.43,4.212c-0.632,0.719-1.263,0.809-2.347,0.271c-1.082-0.537-4.571-1.673-8.708-5.333   c-3.219-2.848-5.393-6.364-6.025-7.441c-0.631-1.075-0.066-1.656,0.475-2.191c0.488-0.482,1.084-1.255,1.625-1.882   c0.543-0.628,0.723-1.075,1.082-1.793c0.363-0.717,0.182-1.344-0.09-1.883c-0.27-0.537-2.438-5.825-3.34-7.977   c-0.902-2.15-1.803-1.792-2.436-1.792c-0.631,0-1.354-0.09-2.076-0.09c-0.722,0-1.896,0.269-2.889,1.344   c-0.992,1.076-3.789,3.676-3.789,8.963c0,5.288,3.879,10.397,4.422,11.113c0.541,0.716,7.49,11.92,18.5,16.223   C58.2,65.771,58.2,64.336,60.186,64.156c1.984-0.179,6.406-2.599,7.312-5.107C68.398,56.537,68.398,54.386,68.129,53.938z">
                    </path>
                    </svg>
                    </div>
                    <div class="chat-text">
                    Start Chat
                    </div>
                    </div>
                <div class="powered-text">
                <p class="powered-text1"><img class="prescribe-logo" src="../assets/prescribe.png"> powered by Prescribe</P>
                </div>
            </div>
            </div>
            <div class="small-icon"></div>
            <div class="chat-buttons">
            <div class="whatsapp-logo">
            <svg viewBox="0 0 90 90" fill="rgb(79, 206, 93)" width="32" height="32">
            <path d="M90,43.841c0,24.213-19.779,43.841-44.182,43.841c-7.747,0-15.025-1.98-21.357-5.455L0,90l7.975-23.522   c-4.023-6.606-6.34-14.354-6.34-22.637C1.635,19.628,21.416,0,45.818,0C70.223,0,90,19.628,90,43.841z M45.818,6.982   c-20.484,0-37.146,16.535-37.146,36.859c0,8.065,2.629,15.534,7.076,21.61L11.107,79.14l14.275-4.537   c5.865,3.851,12.891,6.097,20.437,6.097c20.481,0,37.146-16.533,37.146-36.857S66.301,6.982,45.818,6.982z M68.129,53.938   c-0.273-0.447-0.994-0.717-2.076-1.254c-1.084-0.537-6.41-3.138-7.4-3.495c-0.993-0.358-1.717-0.538-2.438,0.537   c-0.721,1.076-2.797,3.495-3.43,4.212c-0.632,0.719-1.263,0.809-2.347,0.271c-1.082-0.537-4.571-1.673-8.708-5.333   c-3.219-2.848-5.393-6.364-6.025-7.441c-0.631-1.075-0.066-1.656,0.475-2.191c0.488-0.482,1.084-1.255,1.625-1.882   c0.543-0.628,0.723-1.075,1.082-1.793c0.363-0.717,0.182-1.344-0.09-1.883c-0.27-0.537-2.438-5.825-3.34-7.977   c-0.902-2.15-1.803-1.792-2.436-1.792c-0.631,0-1.354-0.09-2.076-0.09c-0.722,0-1.896,0.269-2.889,1.344   c-0.992,1.076-3.789,3.676-3.789,8.963c0,5.288,3.879,10.397,4.422,11.113c0.541,0.716,7.49,11.92,18.5,16.223   C58.2,65.771,58.2,64.336,60.186,64.156c1.984-0.179,6.406-2.599,7.312-5.107C68.398,56.537,68.398,54.386,68.129,53.938z">
            </path>
            </svg>
            </div>
            </div>

        </div>
    `
    $("bot").html(bot);

    // document.getElementById("resultFrame").contentWindow.Reset();

    var show = true;
    //   if(show){
    //       iframe.style.background='transparent'
    //       iframe.style.width='348px'
    //       iframe.style.height='458px'
    //       iframe.style.position='fixed'
    //       iframe.style.bottom='0'
    //       iframe.style.left='0'
    //       iframe.style.border='0'
    //       iframe.style.zIndex='999'
    //   } else {
    //     iframe.style.background='transparent'
    //     iframe.style.width='60px'
    //     iframe.style.height='60px'
    //     iframe.style.position='fixed'
    //     iframe.style.bottom='0'
    //     iframe.style.left='0'
    //     iframe.style.border='0'
    //     iframe.style.zIndex='999'
    //   }

    
    function play() {
      var audio = new Audio('./js/intuition-561.mp3');
      audio.play();
    }
    //Change the number for diff hospital
    $(".whatsapp-button").click(function () {
        window.open(`https://api.whatsapp.com/send?text=Hi&phone=91${phone}`,'',"width=700, height=600");
      });
    
    //function to hide the chat-box
    $(".close").click(function(){
        $(".chat-container").hide();
        show = false;
      });
      //function to show the chat-box
      $(".chat-buttons").click(function(){
          if(show){
            $(".chat-container").hide();
            show = false;
             
          } else {
            $(".chat-container").show();
              show = true;
              play();
          }
      });

      
      play();
    //   if(Boolean(show)){
    //       $(".chat-container").show();
    //     } else {
    //       $(".chat-container").hide();
    //   }
      
    

})